from flask import Flask, render_template

app = Flask(__name__)

data = [
    {'tanggal': '21-12-2023', 'data': 'Sebulan Trakhir', 'topik': [['like', 'feel', 'year', 'day', 'anxiety', 'people', 'want', 'help', 'thought'], 
                                                                   ['people', 'like', 'feel', 'life', 'want', 'know', 'thing', 'would', 'suicide', 'im'], 
                                                                   ['people', 'get', 'even', 'depression', 'year', 'friend', 'time', 'life', 'help', 'anxiety']]},
]

# Colors for the pie chart labels
pie_chart_colors = ['#FF6384', '#36A2EB', '#FFCE56']

@app.route('/')
def index():
    return render_template('index.html', data=data, pie_chart_colors=pie_chart_colors)

if __name__ == '__main__':
    app.run(debug=True)