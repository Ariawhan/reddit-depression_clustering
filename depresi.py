import praw
import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from gensim.models import Word2Vec
from wordcloud import WordCloud
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score, silhouette_samples, calinski_harabasz_score, davies_bouldin_score
from sklearn.model_selection import ParameterGrid
from sklearn import cluster

import os
os.environ['PRAGMA_NO_VERIFY_HTTPS'] = 'true'

class RedditDepresiClustering:
    def __init__(self, client_id, client_secret, user_agent, subreddit_names, time_period, proxy):
        self.reddit = praw.Reddit(
            client_id=client_id,
            client_secret=client_secret,
            user_agent=user_agent
        )
        self.subreddit_names = subreddit_names
        self.all_data = []
        self.n_cluster = 3
        self.time_period = time_period
        self.proxy = proxy
      
    # def set_proxy(self):
    #     os.environ['HTTP_PROXY'] = self.proxy
    #     os.environ['HTTPS_PROXY'] = self.proxy
    
    def scrape_reddit_data(self):
        # if self.proxy:
        #     self.set_proxy()

        for subreddit_name in self.subreddit_names:
            subreddit = self.reddit.subreddit(subreddit_name)
            subreddit_posts = subreddit.top(time_filter=self.time_period, limit=None)

            for post in subreddit_posts:
                post_data = {
                    'title': post.title,
                    'content': post.selftext,
                    'score': post.score
                }
                self.all_data.append(post_data)

        self.all_data = pd.DataFrame(self.all_data)

    def preprocess_data(self):
        self.all_data = self.all_data.dropna(subset=['content']).reset_index(drop=True)
        self.all_data = self.all_data.dropna(subset=['title']).reset_index(drop=True)

        #Perbaiki Outlayer
        negative_or_zero_scores = self.all_data[self.all_data['score'] <= 0].shape[0]
        negative_or_zero_scores
        offset = np.abs(self.all_data['score'].min()) + 1
        self.all_data['score'] = np.log(self.all_data['score'] + offset)

        #Ambil data tinggi
        # Mengitung mean dan std dari score
        mean_score = self.all_data['score'].mean()
        std_deviation = self.all_data['score'].std()

        # Set threshold
        threshold = mean_score + 2 * std_deviation

        high_data = self.all_data[self.all_data['score'] >= threshold]
        weak_data = self.all_data[self.all_data['score'] < threshold]

        return high_data, weak_data    

    def preprocess_text(self, text):
        text = re.sub(r'(\\x[0-9a-fA-Z]{2})', '', text)
        # Remove URL
        text = re.sub(r'https?://\S+|www\.\S+', ' ', text)

        # Remove Mentions
        text = re.sub(r'@\w+', ' ', text)

        # Remove Punctuation, Digits, Symbols
        text = re.sub(r'[^\w\s]|[\d_]', ' ', text)

        # Remove HTML tags
        text = re.sub(r'<.*?>', ' ', text)

        # Remove Hashtags
        text = re.sub(r'#\w+', ' ', text)

        # Convert text to lowercase
        text = text.lower()

        # Tokenize the text
        words = word_tokenize(text)

        # Remove stopwords
        stop_words = set(stopwords.words('english'))
        words = [word for word in words if word not in stop_words]

        # Lemmatize the words
        lemmatizer = WordNetLemmatizer()
        words = [lemmatizer.lemmatize(word) for word in words]

        # Join the words back into a single string
        preprocessed_text = ' '.join(words)

        return preprocessed_text

    def feature_extraction(self, data):
        docs_high = data['clean_text'].to_list()
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_matrix = tfidf_vectorizer.fit_transform(docs_high)
        # pecah term
        feature_names = tfidf_vectorizer.get_feature_names_out()
        # Extract keywords with the highest TF-IDF scores for each document
        n_keywords = 5  # Number of top keywords to extract for each document

        keywords = []
        for i in range(len(docs_high)):
            # Sort the TF-IDF scores for the current document
            tfidf_scores = tfidf_matrix[i, :].toarray()[0]
            top_keyword_indices = tfidf_scores.argsort()[-n_keywords:][::-1]

            # Get the top keywords
            top_keywords = [feature_names[idx] for idx in top_keyword_indices]

            # Join the keywords into a single string
            keywords.append(', '.join(top_keywords))

        # Add the Keywords column to the DataFrame
        data['Keywords'] = keywords

        return data

    def build_corpus(self, data):
        corpus = []
        for sentence in data.iteritems():
            keyword_string = sentence[1]['Keywords']
            # Split the comma-separated keywords and remove leading/trailing whitespace
            keywords = [keyword.strip() for keyword in keyword_string.split(",")]
            corpus.append(keywords)
        return corpus

    def clustering(self, corpus):
        high_model = Word2Vec(corpus, vector_size=100, min_count=1)
        # fit a 2D PCA model to the vectors
        vectors = high_model.wv[high_model.wv.key_to_index]
        words = list(high_model.wv.key_to_index.keys())
        pca = PCA(n_components=2)  # Set the desired number of components (e.g., 2 for 2D visualization)
        PCA_result = pca.fit_transform(vectors)

        # Prepare a DataFrame
        PCA_result_df = pd.DataFrame(PCA_result, columns=['x_values', 'y_values'])
        PCA_result_df['word'] = words
        PCA_data_complete = PCA_result_df[['word', 'x_values', 'y_values']]

        #Cluster
        clf = cluster.KMeans(n_clusters=self.n_cluster,
            max_iter=100,
            init='k-means++',
            n_init=1)
        labels = clf.fit_predict(PCA_result)

        return PCA_result_df, labels

    def run_analysis(self):
        self.scrape_reddit_data()
        high_data, weak_data = self.preprocess_data()
        high_data['clean_text'] = high_data['content'].apply(self.preprocess_text)
        weak_data['clean_text'] = weak_data['content'].apply(self.preprocess_text)
        data_with_keyword = self.feature_extraction(high_data)
        corpus_high, labels = self.build_corpus(data_with_keyword['Keywords'])
        print(corpus_high, labels)  # Change this line to print(corpus, labels)


        

# Example usage:
reddit_analysis = RedditDepresiClustering(
    client_id='PTWqzreeXBM1Y44pt1kLyA',
    client_secret='A5SP8SrVpB0jZMBsswDqfHlAbz-NQQ',
    user_agent='YOUR_APP_NAME',
    subreddit_names=['depression', 'mentalhealth', 'Anxiety', 'SuicideWatch'],
    time_period = 'month',
    proxy='24.249.199.4:4145'
)

reddit_analysis.run_analysis()
